![Logo](https://camo.githubusercontent.com/b760dcbf9274953f43e767d6b5255f05cb9c7f9b518a4e779f0a0ec1467bb0a8/68747470733a2f2f692e696d6775722e636f6d2f747a5042614c4f2e706e67)

# Laravel & Quasar

CRUD of articles with user permissions and access control, on the backend side we use Laravel to create the API that we are going to consume from the client in Quasar

## Environment Variables

Before starting, make sure you have the database created, the environment variables are in the .env file in the root of the project. (Copy the file .env.example and rename .env)

`DB_CONNECTION`

`DB_HOST`

`DB_PORT`

`DB_DATABASE`

`DB_USERNAME`

`DB_PASSWORD`

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/oscarfm24/api-articles-laravel-quasar
```

Go to the project directory

```bash
  cd your_route/api-articles-laravel-quasar
```

1. **Install dependencies of the laravel(Backend):**

```bash
  composer install
```

```bash
  composer dump-autoload
```

```bash
  php artisan key:generate
```

```bash
  php artisan migrate --seed
```

```bash
  php artisan passport:install
```

_Backend server running:_

```bash
  php artisan serv
```

2. **Install dependencies of the client:**

```bash
  cd client
  npm install
```

_Frontend server running:_

```bash
  npm run dev
```

Both servers must be running.

## Tech Stack

**Client:** Quasar, TailwindCSS

**Server:** Laravel

**URL:** http://localhost:8080/
