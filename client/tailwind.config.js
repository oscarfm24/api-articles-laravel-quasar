/** @type {import('tailwindcss').Config} */
module.exports = {
  prefix: "tw-",
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}", // Ajusta las rutas según tu estructura de proyecto
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};
