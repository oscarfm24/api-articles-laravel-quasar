import axios from "axios";

const API_URL = process.env.API_URL;

const token = localStorage.getItem("authToken");

const apiUsers = axios.create({
  baseURL: API_URL,
  headers: {
    Accept: "application/vnd.api+json",
    "Content-Type": "application/vnd.api+json",
    Authorization: `Bearer ${token}`,
  },
});

const getUsers = async (path) => {
  try {
    const response = await apiUsers.get(`${API_URL}/${path}`);
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

const addUser = async (path, data) => {
  try {
    const response = await apiUsers.post(`${API_URL}/${path}`, data);
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

const editUser = async (path, id, data) => {
  try {
    const response = await apiUsers.patch(`${API_URL}/${path}/${id}`, data);
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

const deleteUser = async (path, id) => {
  try {
    const response = await apiUsers.delete(`${API_URL}/${path}/${id}`);
    return response.data;
  } catch (error) {
    throw error;
  }
};

const getUsersDeleted = async () => {
  try {
    const response = await apiUsers.get(`${API_URL}/paper`);
    console.log(response.data.users.data);
    return response.data.users.data;
  } catch (error) {
    throw error;
  }
};

const deleteUserForce = async (id) => {
  try {
    const response = await apiUsers.delete(`${API_URL}/users/${id}/force`);
    return response.data;
  } catch (error) {
    throw error;
  }
};

const restoreUser = async (id) => {
  try {
    const response = await apiUsers.patch(
      `${API_URL}/users/${id}/restore`,
      {},
      {
        headers: {
          Accept: "application/vnd.api+json",
          "Content-Type": "application/vnd.api+json",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    console.log(response);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export {
  getUsers,
  addUser,
  editUser,
  deleteUser,
  getUsersDeleted,
  deleteUserForce,
  restoreUser,
};
