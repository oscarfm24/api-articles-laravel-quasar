import axios from "axios";

const API_URL = process.env.API_URL;
const CLIENT_ID = process.env.API_PASSPORT_CLIENT_ID;
const CLIENT_SECRET = process.env.API_PASSPORT_CLIENT_SECRET;

const apiClient = axios.create({
  baseURL: API_URL,
  headers: {
    Accept: "application/vnd.api+json",
    "Content-Type": "application/vnd.api+json",
  },
});

const login = async (data) => {
  const body = {
    grant_type: "password",
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
    email: data.email,
    password: data.password,
  };

  try {
    const response = await apiClient.post(`${API_URL}/login`, body);
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

const register = async (data) => {
  const body = {
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
    name: `${data.name} ${data.last_name}`,
    email: data.email,
    password: data.password,
    password_confirmation: data.password_confirmation,
  };

  try {
    const response = await apiClient.post(`${API_URL}/register`, body);
    return response;
  } catch (error) {
    throw error;
  }
};

const validateToken = async (token) => {
  try {
    const response = await apiClient.get(
      `${API_URL}/token/validate`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/vnd.api+json",
        },
      }
    );
    return response.data;
  } catch (error) {
    throw error;
  }
};

const logout = async () => {
  const token = localStorage.getItem("authToken");
  try {
    const response = await apiClient.post(
      `${API_URL}/logout`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/vnd.api+json",
          "Content-Type": "application/vnd.api+json",
        },
      }
    );
    return response.data;
  } catch (error) {
    throw error;
  }
};

export { login, register, validateToken, logout };
