import axios from "axios";

const API_URL = `${process.env.API_URL}/articles`;

const token = localStorage.getItem("authToken");

const apiClient = axios.create({
  baseURL: API_URL,
  headers: {
    Accept: "application/vnd.api+json",
    "Content-Type": "application/vnd.api+json",
    Authorization: `Bearer ${token}`,
  },
});

const getArticles = async () => {
  try {
    const response = await apiClient.get(API_URL);
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

const getArticle = async (id) => {
  try {
    const response = await apiClient.get(`${API_URL}/${id}`);
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

const addArticle = async (data) => {
  try {
    const response = await apiClient.post(API_URL, data);
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

const editArticle = async (id, data) => {
  try {
    const response = await apiClient.patch(`${API_URL}/${id}`, data);
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

const deleteArticle = async (id) => {
  try {
    const response = await apiClient.delete(`${API_URL}/${id}`);
    return response.data;
  } catch (error) {
    throw error;
  }
};

const toggleLikeArticle = async (articleId) => {
  try {
    const response = await apiClient.post(`${API_URL}/like`, { articleId });
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

const exportArticles = async () => {
  try {
    const response = await apiClient.get(
      `${process.env.API_URL}/export/articles`,
      {
        responseType: "blob",
      }
    );

    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");

    link.href = url;
    link.setAttribute("download", "articles.xlsx");

    document.body.appendChild(link);

    link.click();
    link.remove();

    return "Export successfully";
  } catch (error) {
    throw error;
  }
};

const getArticlesDeleted = async () => {
  try {
    const response = await apiClient.get(`${process.env.API_URL}/paper`);
    return response.data.articles.data;
  } catch (error) {
    throw error;
  }
};

const deleteArticleForce = async (id) => {
  try {
    const response = await apiClient.delete(`${API_URL}/${id}/force`);
    return response.data;
  } catch (error) {
    throw error;
  }
};

const restoreArticle = async (id) => {
  try {
    const response = await apiClient.patch(
      `${API_URL}/${id}/restore`,
      {},
      {
        headers: {
          Accept: "application/vnd.api+json",
          "Content-Type": "application/vnd.api+json",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    console.log(response);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export {
  getArticlesDeleted,
  getArticles,
  getArticle,
  addArticle,
  editArticle,
  deleteArticle,
  exportArticles,
  toggleLikeArticle,
  deleteArticleForce,
  restoreArticle,
};
