import { boot } from "quasar/wrappers";
import { useAuthStore } from "src/store/auth";

export default boot(({ app }) => {
  app.directive("if-rol", {
    mounted(el, binding) {
      const authStore = useAuthStore();
      const roles = binding.value.split("|").map((role) => role.trim());

      if (!authStore.isUserRole(roles)) {
        el.style.display = "none";
      } else {
        el.style.display = "";
      }
    },
    updated(el, binding) {
      const authStore = useAuthStore();
      const roles = binding.value.split("|").map((role) => role.trim());

      if (!authStore.isUserRole(roles)) {
        el.style.display = "none";
      } else {
        el.style.display = "";
      }
    },
  });

  app.directive("if-can", {
    mounted(el, binding) {
      const requiredPermissions = binding.value;
      const authStore = useAuthStore();
      const hasPermission = requiredPermissions.some((permission) =>
        authStore.permissions.includes(permission)
      );

      if (!hasPermission) {
        el.style.display = "none";
      } else {
        el.style.display = "";
      }
    },
    updated(el, binding) {
      const requiredPermissions = binding.value;
      const authStore = useAuthStore();
      const hasPermission = requiredPermissions.some((permission) =>
        authStore.permissions.includes(permission)
      );

      if (!hasPermission) {
        el.style.display = "none";
      } else {
        el.style.display = "";
      }
    },
  });
});
