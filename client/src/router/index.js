import axios from "axios";
import { useAuthStore } from "src/store/auth";
import { route } from "quasar/wrappers";
import {
  createRouter,
  createMemoryHistory,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";
import routes from "./routes";

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === "history"
    ? createWebHistory
    : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE),
  });

  Router.beforeEach(async (to, from, next) => {
    const API_URL = process.env.API_URL;
    const authStore = useAuthStore();
    const token = authStore.authToken;
    const userRole = authStore.userRole;
    const isAuthPage = to.path === "/" || to.path === "/register";
    const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);

    // Si no requiere autenticación y es página de autenticación sin token, permite la navegación.
    if (!requiresAuth && !token && isAuthPage) {
      return next();
    }

    // Si no requiere autenticación y no es página de autenticación, permite la navegación.
    if (!requiresAuth && !isAuthPage) {
      return next();
    }

    // Si requiere autenticación pero no hay token, redirige a la página principal.
    if (requiresAuth && !token) {
      return next("/");
    }

    // Si hay token, intenta validar.
    if (token) {
      try {
        await axios.get(`${API_URL}/token/validate`, {
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/vnd.api+json",
            "Content-Type": "application/vnd.api+json",
          },
        });

        if (isAuthPage) {
          switch (userRole) {
            case "root":
              return next("/admins");
            case "admin":
              return next("/admins");
            case "writer":
              return next("/articles");
            default:
              return next("/articles");
          }
        }

        if (
          userRole !== "root" &&
          userRole !== "admin" &&
          (to.path === "/admins" ||
            to.path === "/writers" ||
            to.path === "/users" ||
            to.path === "/bin/users" ||
            to.path === "/bin/articles")
        ) {
          return next("/articles");
        }

        return next();
      } catch (error) {
        console.error("Error token: ", error);
        localStorage.removeItem("authToken");
        return next("/");
      }
    }
  });

  return Router;
});
