const routes = [
  {
    path: "/",
    component: () => import("layouts/AuthLayout.vue"),
    children: [
      { path: "", component: () => import("pages/auth/LoginPage.vue") },
    ],
  },

  {
    path: "/register",
    component: () => import("layouts/AuthLayout.vue"),
    children: [
      { path: "", component: () => import("pages/auth/RegisterPage.vue") },
    ],
  },

  {
    path: "/admins",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/HomePage.vue") }],
    meta: { requiresAuth: true },
  },

  {
    path: "/writers",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/HomePage.vue") }],
    meta: { requiresAuth: true },
  },

  {
    path: "/users",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/HomePage.vue") }],
    meta: { requiresAuth: true },
  },

  {
    path: "/articles",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/articles/ArticlesPage.vue") },
    ],
    meta: { requiresAuth: true },
  },
  {
    path: "/articles/view/:id",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/articles/ArticleViewPage.vue"),
      },
    ],
    meta: { requiresAuth: true },
  },
  {
    path: "/bin/users",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/bin/UsersBin.vue") }],
    meta: { requiresAuth: true },
  },
  {
    path: "/bin/articles",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/bin/ArticlesBin.vue") },
    ],
    meta: { requiresAuth: true },
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
