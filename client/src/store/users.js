import { defineStore } from "pinia";
import {
  getUsers,
  addUser,
  editUser,
  deleteUser,
  getUsersDeleted,
  deleteUserForce,
  restoreUser,
} from "src/services/usersServices.js";

export const useUsersStore = defineStore("users", {
  state: () => ({
    users: [],
    usersError: null,
  }),
  actions: {
    async fetchUsers(path) {
      this.usersError = null;
      try {
        const data = await getUsers(path);
        this.users = data;
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async addUser(path, userData) {
      this.usersError = null;
      try {
        const newUserData = await addUser(path, userData);
        this.users.push(newUserData);
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async updateUser(path, id, userData) {
      this.usersError = null;
      try {
        const updatedUser = await editUser(path, id, userData);
        const index = this.users.findIndex((user) => user.id === id);
        if (index !== -1) {
          this.users[index] = updatedUser;
        }
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async removeUser(path, userId) {
      this.usersError = null;
      try {
        await deleteUser(path, userId);
        this.users = this.users.filter((user) => user.id !== userId);
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async getUsersRemove() {
      this.usersError = null;
      try {
        const data = await getUsersDeleted();
        this.users = data;
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async userRemoveForce(userId) {
      this.usersError = null;
      try {
        await deleteUserForce(userId);
        this.users = this.users.filter((user) => user.id !== userId);
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async restoreUser(userId) {
      this.usersError = null;
      try {
        await restoreUser(userId);
        this.users = this.users.filter((user) => user.id !== userId);
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    handleError(error) {
      if (error.response) {
        if (error.response.data.errors) {
          const errorMessages = [];
          for (const field in error.response.data.errors) {
            if (Object.hasOwnProperty.call(error.response.data.errors, field)) {
              const fieldErrors = error.response.data.errors[field];
              fieldErrors.forEach((errorMsg) => {
                errorMessages.push(`${field}: ${errorMsg}`);
              });
            }
          }
          const errorsString = errorMessages.join(", ");
          this.usersError = `Error: ${error.response.data.message} - ${error.response.status}: ${errorsString}`;
        } else {
          this.usersError = `Error: ${error.response.data.message} - ${error.response.status}`;
        }
      } else if (error.request) {
        this.usersError = "No se pudo establecer conexión con el servidor";
      } else {
        this.usersError = "Error al procesar la solicitud";
      }
    },
  },
});
