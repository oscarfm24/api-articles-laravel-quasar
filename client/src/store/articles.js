import { defineStore } from "pinia";
import {
  getArticles,
  getArticle,
  addArticle,
  editArticle,
  deleteArticle,
  toggleLikeArticle,
  getArticlesDeleted,
  deleteArticleForce,
  restoreArticle,
} from "src/services/articlesServices.js";

export const useArticleStore = defineStore("articles", {
  state: () => ({
    articles: [],
    articleError: null,
    article: null,
  }),
  actions: {
    async fetchArticles() {
      this.articles = null;
      this.articleError = null;
      try {
        const data = await getArticles();
        this.articles = data;
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async getArticle(id) {
      this.articleError = null;
      try {
        this.article = await getArticle(id);
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async addArticle(articleData) {
      this.articleError = null;
      try {
        const newArticleData = await addArticle(articleData);
        this.articles.push(newArticleData);
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async updateArticle(id, articleData) {
      this.articleError = null;
      try {
        this.article = await editArticle(id, articleData);
        const index = this.articles.findIndex((article) => article.id === id);
        if (index !== -1) {
          this.articles[index] = this.article;
        }
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async removeArticle(articleId) {
      this.articleError = null;
      try {
        await deleteArticle(articleId);
        this.articles = this.articles.filter(
          (article) => article.id !== articleId
        );
        this.article = null;
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async likeToggleArticle(articleId) {
      this.articleError = null;
      try {
        this.article = await toggleLikeArticle(articleId);

        const index = this.articles.findIndex(
          (article) => article.id === articleId
        );

        if (index !== -1) {
          this.articles[index] = this.article;
        }
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async getArticlesRemove() {
      this.articleError = null;
      try {
        const data = await getArticlesDeleted();
        this.articles = data;
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async articleRemoveForce(articleId) {
      this.articleError = null;
      try {
        await deleteArticleForce(articleId);
        this.articles = this.articles.filter(
          (article) => article.id !== articleId
        );
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    async restoreArticle(articleId) {
      this.articleError = null;
      try {
        await restoreArticle(articleId);
        this.articles = this.articles.filter(
          (article) => article.id !== articleId
        );
      } catch (error) {
        this.handleError(error);
        throw error;
      }
    },
    handleError(error) {
      if (error.response) {
        if (error.response.data.errors) {
          const errorMessages = [];
          for (const field in error.response.data.errors) {
            if (Object.hasOwnProperty.call(error.response.data.errors, field)) {
              const fieldErrors = error.response.data.errors[field];
              fieldErrors.forEach((errorMsg) => {
                errorMessages.push(`${field}: ${errorMsg}`);
              });
            }
          }

          const errorsString = errorMessages.join(", ");
          this.articleError = `Error: ${error.response.data.message} - ${error.response.status}: ${errorsString}`;
        } else {
          this.articleError = `Error: ${error.response.data.message} - ${error.response.status}`;
        }
      } else if (error.request) {
        this.articleError = "No se pudo establecer conexión con el servidor";
      } else {
        this.articleError = "Error al procesar la solicitud";
      }
    },
  },
});
