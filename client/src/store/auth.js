import { defineStore } from "pinia";
import {
  login,
  register,
  validateToken,
  logout,
} from "src/services/authServices";

export const useAuthStore = defineStore("auth", {
  state: () => {
    let permissions;
    try {
      permissions = JSON.parse(localStorage.getItem("permissions")) || null;
    } catch (e) {
      console.error("Error parsing permissions from localStorage:", e);
      permissions = null;
    }

    return {
      authToken: localStorage.getItem("authToken") || null,
      userRole: localStorage.getItem("userRole") || null,
      userName: localStorage.getItem("userName") || null,
      permissions,
      authError: null,
    };
  },
  getters: {
    isAuthenticated(state) {
      return !!state.authToken;
    },
    isUserRole: (state) => (roles) => {
      return roles.includes(state.userRole);
    },
    isPermissions: (state) => (permissions) => {
      if (state.permissions) return state.permissions.includes(permissions);

      return false;
    },
    isUserName: (state) => (name) => {
      return state.userName === name;
    },
  },
  actions: {
    async signIn(dataLoginForm) {
      this.authError = null;
      try {
        const { token, role, name, permissions } = await login(dataLoginForm);
        this._setToken(token);
        this._setUserName(name);
        if (role !== undefined) {
          this._setUserRole(role);
        }
        if (permissions !== undefined) {
          this._setPermissions(permissions);
        }
      } catch (error) {
        this._handleAuthError(error);
        throw error;
      }
    },
    async register(dataRegisterForm) {
      this.authError = null;
      try {
        const { token, role, name } = await register(dataRegisterForm);
        this._setToken(token);
        this._setUserRole(role);
        this._setUserName(name);
      } catch (error) {
        this._handleAuthError(error);
        throw error;
      }
    },
    async validateTokenAndSetUserRole() {
      if (!this.authToken) {
        throw new Error("No auth token available");
      }
      try {
        const { role, name, permissions } = await validateToken(this.authToken);
        this.userRole = role;
        this.userName = name;
        this._setPermissions(permissions);
      } catch (error) {
        this._handleAuthError(error);
        this.logout();
        throw error;
      }
    },
    async logout() {
      try {
        await logout();
        localStorage.removeItem("authToken");
        localStorage.removeItem("userName");
        localStorage.removeItem("userRole");
        localStorage.removeItem("permissions");
        this.authToken = null;
        this.userRole = null;
        this.userName = null;
        this.permissions = null;
      } catch (error) {}
    },
    _setToken(token) {
      this.authToken = token;
      localStorage.setItem("authToken", token);
    },
    _setUserRole(role) {
      this.userRole = role;
      localStorage.setItem("userRole", role);
    },
    _setPermissions(permissions) {
      this.permissions = permissions;
      localStorage.setItem("permissions", JSON.stringify(permissions));
    },
    _setUserName(name) {
      this.userName = name;
      localStorage.setItem("userName", name);
    },
    _handleAuthError(error) {
      if (error.response) {
        this.authError = `Error ${error.response.status}: ${error.response.data.message}`;
      } else if (error.request) {
        this.authError = "No se pudo establecer conexión con el servidor";
      } else {
        this.authError = "Error al procesar la solicitud";
      }
    },
  },
});
