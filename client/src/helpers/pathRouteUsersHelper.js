export function determinePathRoute(path) {
  let response;
  switch (path) {
    case "/admins":
      response = {
        route: "admins",
        nameSection: "Admins",
        titleCreate: "Nuevo admin",
        roleCreate: "admin",
      };
      return response;
    case "/users":
      response = {
        route: "users",
        nameSection: "Usuarios",
        titleCreate: "Nuevo usuario",
        roleCreate: "user",
      };
      return response;
    case "/writers":
      response = {
        route: "writers",
        nameSection: "Escritores",
        titleCreate: "Nuevo escritor",
        roleCreate: "writer",
      };
      return response;
    default:
      return null;
  }
}
