<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class LoginTest
 *
 * @package Tests\Feature\Auth
 * @author Oscar Muñoz Franco Web developer
 */

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Set up the test case.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('passport:install');

        Passport::actingAs(
            User::factory()->create([
                'email' => 'oscar@tester.com',
                'password' => 'Oscar.454'
            ])
        );
    }

    /**
     * @test
     * @return void
     */
    public function successful_login(): void
    {
        $this->withExceptionHandling();

        $response = $this->postJson(route('api.v1.login'), [
            'email' => 'oscar@tester.com',
            'password' => 'Oscar.454',
        ], [
            'accept' => 'application/vnd.api+json',
            'content-type' => 'application/vnd.api+json'
        ]);

        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     * @return void
     */
    public function email_is_required(): void
    {
        $this->withExceptionHandling();

        $response = $this->postJson(route('api.v1.login'), [
            'password' => 'Oscar.454',
        ], [
            'accept' => 'application/vnd.api+json',
            'content-type' => 'application/vnd.api+json'
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJsonStructure([
            'message',
            'errors' => [
                'email',
            ],
        ]);

        $response->assertJson([
            'message' => 'The email field is required.',
            'errors' => [
                'email' => [
                    'The email field is required.'
                ],
            ],
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function password_is_required(): void
    {
        $this->withExceptionHandling();

        $response = $this->postJson(route('api.v1.login'), [
            'email' => 'oscar@tester.com',
        ], [
            'accept' => 'application/vnd.api+json',
            'content-type' => 'application/vnd.api+json'
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJsonStructure([
            'message',
            'errors' => [
                'password',
            ],
        ]);

        $response->assertJson([
            'message' => 'The password field is required.',
            'errors' => [
                'password' => [
                    'The password field is required.'
                ],
            ],
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function email_is_invalid(): void
    {
        $this->withExceptionHandling();

        $response = $this->postJson(route('api.v1.login'), [
            'email' => '2oscar@tester.com',
            'password' => 'Oscar.454',
        ], [
            'accept' => 'application/vnd.api+json',
            'content-type' => 'application/vnd.api+json'
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJsonStructure([
            'message',
            'errors' => [
                'email',
            ],
        ]);

        $response->assertJson([
            'message' => 'The selected email is invalid.',
            'errors' => [
                'email' => [
                    'The selected email is invalid.'
                ],
            ],
        ]);
    }
}
