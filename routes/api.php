<?php

use App\Http\Controllers\Api\AdminController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\TokenController;
use App\Http\Controllers\Api\WriterController;
use App\Http\Controllers\Api\ArticleController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\LogoutController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\LikeController;
use App\Http\Controllers\Api\PaperController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', LoginController::class)->name('login');
Route::post('logout', LogoutController::class)->name('logout');
Route::post('register', RegisterController::class)->name('register');
Route::get('token/validate', TokenController::class)->name('token.validate');


Route::apiResource('users', UserController::class)->middleware('auth:api')->names('users');
Route::group(['controller' => UserController::class, 'prefix' => 'users', 'as' => 'users.', 'middleware' => 'auth:api'], function () {
    Route::delete('{id}/force', 'forceDestroy')->name('destroy.force');
    Route::patch('{id}/restore', 'restore')->name('restore');
    Route::get('export', 'export')->name('export');
});

Route::apiResource('admins', AdminController::class)->middleware('auth:api')->names('admins');
Route::group(['controller' => AdminController::class, 'prefix' => 'admins', 'as' => 'admins.', 'middleware' => 'auth:api'], function () {
    Route::delete('{id}/force', 'forceDestroy')->name('destroy.force');
    Route::patch('{id}/restore', 'restore')->name('restore');
});

Route::apiResource('writers', WriterController::class)->middleware('auth:api')->names('writers');
Route::group(['controller' => WriterController::class, 'prefix' => 'writers', 'as' => 'writers.', 'middleware' => 'auth:api'], function () {
    Route::delete('{id}/force', 'forceDestroy')->name('destroy.force');
    Route::patch('{id}/restore', 'restore')->name('restore');
});

Route::apiResource('articles', ArticleController::class)->middleware('auth:api')->names('articles');
Route::group(['controller' => ArticleController::class, 'prefix' => 'articles', 'as' => 'articles.', 'middleware' => 'auth:api'], function () {
    Route::delete('{id}/force', 'forceDestroy')->name('destroy.force');
    Route::patch('{id}/restore', 'restore')->name('restore');
});
Route::get('/export/articles', [ArticleController::class, 'export'])->name('articles.export');

Route::post('articles/like', LikeController::class)->middleware('auth:api')->name('articles.like');

Route::get('paper', PaperController::class)->middleware('auth:api')->name('paper');
