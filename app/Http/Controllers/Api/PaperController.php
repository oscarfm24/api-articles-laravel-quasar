<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\UserCollection;

class PaperController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke()
    {
        $users = User::onlyTrashed()->with('roles')->get();
        $articles = Article::onlyTrashed()->get();

        return response()->json([
            'users' => UserCollection::make($users),
            'articles' => ArticleCollection::make($articles),
        ]);
    }
}
