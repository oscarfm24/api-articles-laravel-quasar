<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Article;
use Illuminate\Http\Response;
use App\Exports\ArticlesExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Resources\ArticleResource;
use App\Http\Requests\SaveArticleRequest;
use App\Http\Resources\ArticleCollection;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): ArticleCollection
    {
        return ArticleCollection::make(Article::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SaveArticleRequest $request): ArticleResource
    {
        $this->authorize('create', User::class);

        if ($request->user()->hasRole('root') || ($request->user()->hasRole('admin') && $request->user()->hasPermissionTo('create-articles'))) {
            $article = Article::create($request->only('title', 'slug', 'content'));
        }

        if ($request->user()->hasRole('writer') && $request->user()->hasPermissionTo('create-articles')) {
            $article = $request->user()->articles()->create($request->all());
        }


        return ArticleResource::make($article);
    }

    /**
     * Display the specified resource.
     */
    public function show(Article $article): ArticleResource
    {
        $article->increment('views');
        return ArticleResource::make($article);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Article $article, SaveArticleRequest $request): ArticleResource
    {
        $this->authorize('update', $article);

        $article->update($request->all());

        return ArticleResource::make($article);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Article $article): Response
    {
        $this->authorize('delete', $article);

        $article->delete();

        return response()->noContent();
    }

    /**
     * Restore the specified resource from storage.
     */
    public function restore(string $id): ArticleResource
    {
        $article = Article::withTrashed()->findOrFail($id);

        $this->authorize('restore', $article);

        $article->restore();

        return ArticleResource::make($article);
    }

    /**
     * Permanently delete the specified resource from storage.
     */
    public function forceDestroy(string $id): Response
    {
        $article = Article::withTrashed()->findOrFail($id);

        $this->authorize('force-delete', $article);

        $article->forceDelete();

        return response()->noContent();
    }

    /**
     * Export the resource.
     */
    public function export()
    {
        return Excel::download(new ArticlesExport, 'articles.xlsx');
    }
}
