<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\AdminResource;
use App\Http\Requests\SaveAdminRequest;
use App\Http\Resources\AdminCollection;
use App\Mail\TempPasswordUserMail;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): AdminCollection
    {
        $this->authorize('admin.view-any', User::class);

        $admins = User::role('admin')->where('id', '!=', request()->user()->id)->get();

        return AdminCollection::make($admins);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SaveAdminRequest $request): AdminResource
    {
        $this->authorize('admin.create', User::class);

        $admin = User::create($request->only('name', 'email'));

        $admin->assignRole('admin')->givePermissionTo($request->permissions);

        try {
            $data = [
                'password' => 'Hola454',
                'url' => 'https://localhost:8000'
            ];

            Mail::to($admin->email)->send(new TempPasswordUserMail($data));
        } catch (\Throwable $th) {
            //throw $th;
        }

        return AdminResource::make($admin);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $admin): AdminResource
    {
        $this->authorize('admin.view', $admin);

        return AdminResource::make($admin);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SaveAdminRequest $request, User $admin): AdminResource
    {
        $this->authorize('admin.update', $admin);

        $admin->update($request->only('name', "email"));

        if (request()->user()->hasRole('root')) {
            $admin->syncRoles($request->rol);
            $admin->syncPermissions($request->permissions);
        }

        return AdminResource::make($admin);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $admin): Response
    {
        $this->authorize('admin.delete', $admin);

        $admin->delete();

        return response()->noContent();
    }

    /**
     * Restore the specified resource from storage.
     */
    public function restore(string $id): AdminResource
    {
        $admin = User::role('admin')->withTrashed()->findOrFail($id);

        $this->authorize('admin.restore', $admin);

        $admin->restore();

        return AdminResource::make($admin);
    }

    /**
     * Permanently delete the specified resource from storage.
     */
    public function forceDestroy(string $id): Response
    {
        $admin = User::role('admin')->withTrashed()->findOrFail($id);

        $this->authorize('admin.force-delete', $admin);

        $admin->forceDelete();

        return response()->noContent();
    }
}
