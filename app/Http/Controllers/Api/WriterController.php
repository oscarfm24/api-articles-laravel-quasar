<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\WriteResource;
use App\Http\Resources\WriteCollection;

class WriterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): WriteCollection
    {
        $writers = User::role('writer')->where('id', '!=', request()->user()->id)->with('articles')->get();

        $this->authorize('view-any', User::class);

        return WriteCollection::make($writers);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): WriteResource
    {
        $this->authorize('create', User::class);

        $writer = User::create($request->only('name', 'email', 'password'));

        $writer->assignRole('writer');

        return WriteResource::make($writer);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $writer): WriteResource
    {
        $this->authorize('view', $writer);

        return WriteResource::make($writer);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(User $writer, Request $request): WriteResource
    {
        $this->authorize('update', $writer);

        $writer->update($request->only('name', 'email', 'password'));

        return WriteResource::make($writer);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $writer): Response
    {
        $this->authorize('delete', $writer);

        $writer->delete();

        return response()->noContent();
    }

    /**
     * Restore the specified resource from storage.
     */
    public function restore(string $id): WriteResource
    {
        $writer = User::role('writer')->withTrashed()->findOrFail($id);

        $this->authorize('restore', $writer);

        $writer->restore();

        return WriteResource::make($writer);
    }

    /**
     * Permanently delete the specified resource from storage.
     */
    public function forceDestroy(string $id): Response
    {
        $writer = User::role('writer')->withTrashed()->findOrFail($id);

        $this->authorize('force-delete', $writer);

        $writer->forceDelete();

        return response()->noContent();
    }
}
