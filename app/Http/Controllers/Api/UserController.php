<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Requests\SaveUserRequest;
use App\Http\Resources\UserCollection;
use App\Http\Requests\UpdateUserRequest;

use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): UserCollection
    {
        $this->authorize('view-any', User::class);

        $user = User::whereDoesntHave('roles')
            ->whereDoesntHave('permissions')
            ->get();

        return UserCollection::make($user);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SaveUserRequest $request): UserResource
    {
        $this->authorize('create', User::class);

        $user = User::create($request->only('name', 'email', 'password'));

        return UserResource::make($user);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user): UserResource
    {
        $this->authorize('view', $user);

        return UserResource::make($user);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(User $user, UpdateUserRequest $request): UserResource
    {
        $this->authorize('update', $user);

        $user->update($request->only('name', 'email', 'password'));

        return UserResource::make($user);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user): Response
    {
        $this->authorize('delete', $user);

        $user->delete();

        return response()->noContent();
    }

    /**
     * Restore the specified resource from storage.
     */
    public function restore(string $id): UserResource
    {
        $user = User::withTrashed()->findOrFail($id);

        $this->authorize('restore', $user);

        $user->restore();

        return UserResource::make($user);
    }

    /**
     * Permanently delete the specified resource from storage.
     */
    public function forceDestroy(string $id): Response
    {
        $user = User::withTrashed()->findOrFail($id);

        $this->authorize('force-delete', $user);

        $user->forceDelete();

        return response()->noContent();
    }

    /**
     * Export the resource.
     */
    public function export()
    {
        $this->authorize('export', User::class);

        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
