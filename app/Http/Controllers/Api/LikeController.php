<?php

namespace App\Http\Controllers\Api;

use App\Models\Like;
use App\Models\Article;
use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleResource;

class LikeController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(): ArticleResource
    {
        $article = Article::findOrFail(request()->articleId);
        $like = $article->likes()->firstWhere('user_id', auth()->id());

        if ($like) {
            $like->delete();
        } else {
            Like::create([
                'user_id' => auth()->id(),
                'article_id' => request()->articleId
            ]);
        }

        return ArticleResource::make($article);
    }
}
