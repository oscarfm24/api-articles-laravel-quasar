<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\SaveUserRequest;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(SaveUserRequest $request)
    {
        try {
            $validatedData = $request->validated();

            $validatedData['password'] = Hash::make($validatedData['password']);

            $user = User::create($validatedData);

            $token = $user->createToken('Token')->accessToken;

            $user->token = $token;

            return response()->json([
                'type' => 'users',
                'id' => (string) $user->id,
                'attributes' => array_filter([
                    'name' => $user->name,
                    'email' => $user->email,
                    'token' => $user->token
                ]),
                'links' => [
                    'self' => route('api.v1.users.show', $user->id),
                ],
            ], 201);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error occurred while registering user: ' . $e->getMessage()], 500);
        }
    }
}
