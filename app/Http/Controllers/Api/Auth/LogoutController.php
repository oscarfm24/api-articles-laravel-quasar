<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke()
    {
        request()->user()->token()->revoke();

        return response()->noContent();
    }
}
