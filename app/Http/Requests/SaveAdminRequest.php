<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SaveAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        if (request()->user() && request()->user()->hasRole('root')) {
            return true;
        }

        return request()->user() && $this->isMethod('patch') && request()->user()->id === $this->route('admin')->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($this->route('admin') ?? null),
            ],
        ];

        if (request()->user() && request()->user()->hasRole('root')) {
            $rules['rol'] = ['required', 'exists:roles,name'];
            $rules['permissions'] = ['required', 'array'];
            $rules['permissions.*'] = ['exists:permissions,name'];
        }

        return $rules;
    }
}
