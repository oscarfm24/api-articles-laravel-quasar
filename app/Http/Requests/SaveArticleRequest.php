<?php

namespace App\Http\Requests;

use App\Rules\Slug;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class SaveArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        if (
            request()->user() &&
            (
                request()->user()->hasRole('root') ||
                request()->user()->hasPermissionTo('create-articles') ||
                request()->user()->hasPermissionTo('update-articles')
            )
        ) {
            return true;
        }



        return request()->user() && (request()->user()->hasRole('root') || request()->user()->hasPermissionTo('create-articles'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'min:4'],
            'slug' => [
                'required',
                'alpha_dash',
                new Slug(),
                //Rule::unique('articles', 'slug')
            ],
            'content' => ['required'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'message' => 'Validation error',
            'errors' => $validator->errors(),
        ], 422));
    }
}
