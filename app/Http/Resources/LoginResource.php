<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'success' => true,
            'data' => array_filter([
                ...$this->resource->only('name', 'email', 'token'),
                'role' => $this->resource->roles->first()?->name ?? null,
                'permissions' => $this->getAllPermissions()->pluck('name')->toArray(),
            ]),
            'message' => 'User login successfully',
        ];
    }
}
