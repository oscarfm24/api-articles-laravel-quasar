<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WriteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'type' => 'writers',
            'id' => (string) $this->resource->getRouteKey(),
            'attributes' => array_filter([
                'name' => $this->resource->name,
                'email' => $this->resource->email,
                'role' => $this->resource->roles->first()?->name ?? null,
                'permissions' => $this->getAllPermissions()->pluck('name')->toArray(),
            ]),
            'relationships' => [
                'articles' => new ArticleCollection($this->whenLoaded('articles')),
            ],
            'links' => [
                'self' => route('api.v1.writers.show', $this->resource)
            ]
        ];
    }
}
