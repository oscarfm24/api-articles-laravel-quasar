<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'type' => 'admins',
            'id' => (string) $this->resource->getRouteKey(),
            'attributes' => array_filter([
                'name' => $this->resource->name,
                'email' => $this->resource->email,
                'role' => $this->resource->roles->first()?->name ?? null,
                'permissions' => $this->getAllPermissions()->pluck('name')->toArray(),
            ]),
            'links' => [
                'self' => route('api.v1.admins.show', $this->resource)
            ]
        ];
    }
}
