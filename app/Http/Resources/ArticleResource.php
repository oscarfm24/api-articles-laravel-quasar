<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user = $request->user();
        $hasLike = $user ? $this->resource->likes()->where('user_id', $user->id)->exists() : false;
        return [
            'type' => 'articles',
            'id' => (string) $this->resource->getRouteKey(),
            'attributes' => array_filter([
                'user_id' => (string) $this->resource->user_id,
                'title' => $this->resource->title,
                'slug' => $this->resource->slug,
                'content' => $this->resource->content,
                'views' => $this->resource->views,
                'like' => $hasLike,
                'like_count' => $this->resource->likes()->count(),
                'belongs_to_user' => $user && $user->id === $this->resource->user_id,
                'created_at' => $this->resource->created_at->format('Y-m-d')
            ]),
            'links' => [
                'self' => route('api.v1.articles.show', $this->resource)
            ]
        ];
    }
}
