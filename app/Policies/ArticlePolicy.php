<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;

class ArticlePolicy
{
    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $user->hasRole('root') || $user->hasPermissionTo('create-articles');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Article $article): bool
    {
        if ($user->hasRole('root') || $user->hasPermissionTo('update-articles')) {
            return true;
        }

        return $user->id === $article->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Article $article): bool
    {
        if ($user->hasRole('root') || $user->hasPermissionTo('delete-articles')) {
            return true;
        }

        return $user->id === $article->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Article $article): bool
    {
        if ($user->hasRole('root') || $user->hasPermissionTo('restore-articles')) {
            return true;
        }

        return $user->id === $article->user_id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, Article $article): bool
    {
        if ($user->hasRole('root') || $user->hasPermissionTo('destroy-articles')) {
            return true;
        }

        return $user->id === $article->user_id;
    }

    /**
     * Determine whether the user can export the model.
     */
    public function export(User $user, User $model): bool
    {
        return $user->hasRole('root') || $user->hasPermissionTo('export-articles');
    }
}
