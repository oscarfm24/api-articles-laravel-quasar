<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\Response;

class AdminPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): Response
    {
        return $user->hasRole(['root', 'admin'])
            ? Response::allow()
            : Response::deny('You do not have permission to view users 222.');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, User $model = null): bool
    {
        if ($user->hasRole('root') && $model->hasRole('admin')) {
            return true;
        }

        return $user->id === $model->id && $user->hasRole('admin');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $user->hasRole('root');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, User $model): bool
    {
        if ($user->hasRole('root') && $model->hasRole('admin')) {
            return true;
        }

        return $user->id === $model->id && $user->hasRole('admin');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, User $model): bool
    {
        if ($user->hasRole('root') && $model->hasRole('admin')) {
            return true;
        }

        return $user->id === $model->id && $user->hasRole('admin');
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, User $model): bool
    {
        if ($user->hasRole('root') && $model->hasRole('admin')) {
            return true;
        }

        return $user->id === $model->id && $user->hasRole('admin');
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, User $model): bool
    {
        if ($user->hasRole('root') && $model->hasRole('admin')) {
            return true;
        }

        return $user->id === $model->id && $user->hasRole('admin');
    }
}
