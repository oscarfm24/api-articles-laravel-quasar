<x-mail::message>
Bienvenido alguien te registro en la app de articulos.

Esta es tu contraseña temporal tendras que cambiar en cuenta entres:
{{ $data['password'] }}

<x-mail::button :url="'{{ $data['url'] }}'">
Cambiar contraseña
</x-mail::button>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
