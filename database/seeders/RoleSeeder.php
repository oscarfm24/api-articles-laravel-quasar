<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::create(['guard_name' => 'api', 'name' => 'root']);

        Role::create(['guard_name' => 'api', 'name' => 'admin']);

        Role::create(['guard_name' => 'api', 'name' => 'writer'])
            ->givePermissionTo(
                'create-articles',
                'update-articles',
                'delete-articles',
                'restore-articles',
                'destroy-articles',
            );
    }
}
