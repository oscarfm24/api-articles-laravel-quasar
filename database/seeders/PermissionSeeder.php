<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        Permission::create(['guard_name' => 'api', 'name' => 'fetch-users']);
        Permission::create(['guard_name' => 'api', 'name' => 'read-users']);
        Permission::create(['guard_name' => 'api', 'name' => 'create-users']);
        Permission::create(['guard_name' => 'api', 'name' => 'update-users']);
        Permission::create(['guard_name' => 'api', 'name' => 'delete-users']);
        Permission::create(['guard_name' => 'api', 'name' => 'restore-users']);
        Permission::create(['guard_name' => 'api', 'name' => 'destroy-users']);
        Permission::create(['guard_name' => 'api', 'name' => 'export-users']);

        Permission::create(['guard_name' => 'api', 'name' => 'assign-roles']);
        Permission::create(['guard_name' => 'api', 'name' => 'assign-permissions']);

        Permission::create(['guard_name' => 'api', 'name' => 'create-articles']);
        Permission::create(['guard_name' => 'api', 'name' => 'update-articles']);
        Permission::create(['guard_name' => 'api', 'name' => 'delete-articles']);
        Permission::create(['guard_name' => 'api', 'name' => 'restore-articles']);
        Permission::create(['guard_name' => 'api', 'name' => 'destroy-articles']);
        Permission::create(['guard_name' => 'api', 'name' => 'export-articles']);
    }
}
