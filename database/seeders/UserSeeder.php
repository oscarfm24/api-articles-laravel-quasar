<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'name' => 'Super Admin',
            'email' => 'root@tester.com',
            'password' => Hash::make('Root.454'),
        ])->assignRole('root');

        User::create([
            'name' => 'Admin',
            'email' => 'admin@tester.com',
            'password' => Hash::make('Admin.454'),
        ])->assignRole('admin');

        User::create([
            'name' => 'Write',
            'email' => 'writer@tester.com',
            'password' => Hash::make('Writer.454'),
        ])->assignRole('writer');

        User::create([
            'name' => 'User',
            'email' => 'user@tester.com',
            'password' => Hash::make('User.454'),
        ]);

        \App\Models\User::factory(3)->create([
            'password' => Hash::make('password'),
        ])->each(function ($user) {
            $user->assignRole('admin');
        });

        \App\Models\User::factory(3)->create([
            'password' => Hash::make('password'),
        ])->each(function ($user) {
            $user->assignRole('writer');
        });

        \App\Models\User::factory(5)->create([
            'password' => Hash::make('password'),
        ]);
    }
}
